export const stylBtn = {
		'width': '100%',
		'margin':'0 auto',
		'borderRadius':'27px !important'
};

export const stylGith = {
		'width': '50%'
};

export const stylGoogle = {
		'width': '48%',
		marginLeft: '2%'

};

export const stylFormLocal = {
		'width': '37%',
		'margin':'0 auto'
};
export const stylSocial = {
		marginTop :'12px'
};

export const styPrincipal = {
	padding: '80px 0'
}

export const inputStyl = {
  'width': '100%'
}