import React from 'react';

import { Redirect } from 'react-router-dom';
import {inputStyl} from '../../assets/styleLogin';
import {styPrincipal} from '../../assets/styleLogin';
import WithEnchancer from '../../enchancers';

const Login = ({ onSubmit,email, password,onChangeEmail,onChangePassword,errorConnected }) => {
	return(
		<div className="authentication col-sm-6 offset-sm-3" style={styPrincipal}>
		<div className="row">
			<div className="col-sm-12">
				<h4 className="text-center">Connectez-vous</h4>
			        <div className="form-group">
			     	<div className="col-sm-12 text-danger">
			     		{console.log(errorConnected)}
			     		{errorConnected === 'ok' ?  <Redirect to='/dashboard' /> : ''}
			     		<input 			     		
			     		type="text" 
			     		className="form-control" 
			     		value={email.value}
				        onChange={onChangeEmail}
			     		name="email" 
			     		style={inputStyl} 
			     		placeholder="Email"/>	
			     	</div>
			    </div>							     					    	
			    <div className="form-group">
			     	<div className="col-sm-12 text-danger">
			     		<input 
			     		type="password" 
			     		className="form-control" 
			     		value={password.value}
				        onChange={onChangePassword}
			     		name="password" 
			     		style={inputStyl} 
			     		placeholder="Mot de passe"
			     		/>	
			     	</div>							     					    	
			    </div>
			    <button 
			    className="btn btn-primary" 
			    style={{'width':'100%'}}
			    onClick={onSubmit}
			    >
			    Se connecter
			    </button>
			</div>
		</div>
	</div>
  	)
}

export default WithEnchancer(Login);