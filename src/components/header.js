import React  from 'react';
import {Link} from 'react-router-dom';

const Header = () =>  {
	// const code = this.props.match.params.id; ajout comme props
	const token = localStorage.getItem("token");
	const style_nav = {
		    'margin': '.5rem 0 0 1.5rem'
	}
	
	return (
	    	<nav className="navbar navbar-expand-lg navbar-light menu sticky-top">
	    		{/*eslint-disable */}
			   <div className="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul className="navbar-nav" style={{'marginLeft':'56%'}}>
				      <li className="nav-item active" style={style_nav}>
				        <Link to="/" className="nav-link">Acceuil</Link>
				      </li>
				      <li className="nav-item" style={style_nav}>
				        <Link to="/info" className="nav-link">Documentation</Link>
				      </li>
				      <li className="nav-item" style={style_nav}>
				        <Link to="/help" className="nav-link">Aide</Link>
				      </li>

				      <li className="nav-item dropdown" style={style_nav}>
				        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          Langue
				        </a>
				        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a href="javascript:void(0)" className="dropdown-item">Francais</a>
				          <a href="javascript:void(0)" className="dropdown-item">Anglais</a>
				        </div>
				      </li>				      
				      	{ !token ? (
				      		<li className="nav-item" style={style_nav}>
				      			<Link to="/login" className="btn btn-danger call" style={{'borderRadius':'50px'}}>Se connecter</Link>
				      		</li>
				      	) : (
				      	<li className="nav-item dropdown" style={{'width':'10%','marginLeft':'27px','marginTop':'3px'}}>
					        <a className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          <img src={require('../assets/image/emily.jpg')} style={{'width':'100%','borderRadius':'50%'}}/>
					        </a>
					        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a className="dropdown-item">
					          	Profile
					          	<i className="fa fa-user" aria-hidden="true"/>
					          	</a>
					          <a className="dropdown-item">
					          	Message
					          	<i className="fa fa-envelope"/>
					          </a>
					          <a className="dropdown-item">
					          Logout
					          <i className="fa fa-power-off"/>
					          </a>
					        </div>
				      	</li>
				      	)}				      
				    </ul>
			   </div>
			</nav>
	)
}


export default Header;