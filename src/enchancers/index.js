import { compose } from "recompose";

import WithAuth from './withAuth';
// import WithSend from './withSend';

export default compose(WithAuth);
