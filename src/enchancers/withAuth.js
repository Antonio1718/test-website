import { withStateHandlers } from "recompose";
import {sendData} from './withSend';

const initialState = {
  email: { value: '' },
  password: { value: '' },
  errorConnected: '',
};

const onChangeEmail = props => event => ({
  email: {
    value: event.target.value,
  }
});

const onChangePassword = props => event => ({
  password: {
    value: event.target.value,
  }
});

const onSubmit = props => event => ({
  errorConnected: sendData(props),
})


const WithAuth = withStateHandlers(initialState, {
  onChangeEmail,
  onChangePassword,
  onSubmit
});

export default WithAuth;