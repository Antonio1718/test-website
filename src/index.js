import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from'react-router-dom';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/popper.js/dist/popper.min.js';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import './assets/style.css';

import Home from './components/home';
import Login from './components/auth/login';
import {RequiredLogin} from './components/auth/requiredLogin';
import Help from './components/help';
import Info from './components/info';
import Dashboard from './components/dashboard';

ReactDOM.render(
		<Router>
			<div className="principale container-fluid">
				<div className="content" >
					<Switch>
						<Route exact path="/" component={Home}/>
						<Route  path="/dashboard" component={Dashboard}/>
						<Route  path="/help" component={Help}/>
						<RequiredLogin  path="/info" component={Info}/>
						<Route  path="/login" component={Login}/>						
					</Switch>
				</div>
			</div>
		</Router>

, document.getElementById('root'));

